const supertest = require('supertest');
const app = require('./index');

describe('Test GET /quote route', () => {
  
  test('/quote/GRU/SCL should return statusCode 200', async () => {
    const response = await supertest(app)
      .get('/quote/GRU/SCL');
    expect(response.statusCode).toEqual(200);
  });

  test('/quote/GRU/SCL should return route GRU,BRC,SCL and price 15', async () => {
    const response = await supertest(app)
      .get('/quote/GRU/SCL');
    expect(response.body.route).toEqual('GRU,BRC,SCL');
    expect(response.body.price).toEqual(15);
  });

  test('/quote/GRU should return statusCode 404', async () => {
    const response = await supertest(app)
      .get('/quote/GRU');
    expect(response.statusCode).toEqual(404);
  });

  test('/quote should return statusCode 404', async () => {
    const response = await supertest(app)
      .get('/quote');
    expect(response.statusCode).toEqual(404);
  });
  
});

describe('POST /route from BRC to BA with price 10', () => {
  
  test('should return statusCode 200', async () => {
    const response = await supertest(app)
      .post('/route')
      .send({
        "from": "BRC",
        "to": "BA",
        "price": 10
      });
    expect(response.statusCode).toEqual(200);
  });

  test('should return route from BRC to BA with price 10', async () => {
    const response = await supertest(app)
      .post('/route')
      .send({
        "from": "BRC",
        "to": "BA",
        "price": 10
      });
      expect(response.body.from).toEqual('BRC');
      expect(response.body.to).toEqual('BA');
      expect(response.body.price).toEqual(10);
  });

  test('empty body should return statusCode 400', async () => {
    const response = await supertest(app)
      .post('/route')
      .send();
    expect(response.statusCode).toEqual(400);
  });

  describe('Test GET /quote/BRC/BA', () => {
  
    test('should return statusCode 200', async () => {
      const response = await supertest(app)
        .get('/quote/BRC/BA');
      expect(response.statusCode).toEqual(200);
    });
  
    test('should return route BRC,BA and price 10', async () => {
      const response = await supertest(app)
        .get('/quote/BRC/BA');
      expect(response.body.route).toEqual('BRC,BA');
      expect(response.body.price).toEqual(10);
    });
  
  });

  

});