// const fs = require('fs');
// const csv = require('csv-parser');

// const loadRoutesRecords = async () => {
//   return new Promise((resolve, reject) => {
//     fs.createReadStream('./data/routes.csv')
//     .pipe(csv())
//     .on('data', (row) => {
//       routesRecords.push({
//         from: row['from'],
//         to: row['to'],
//         price: row['price']
//       });
//     })
//     .on('end', () => {
//       resolve(routesRecords);
//     })
//     .on('error', reject);
//   });
// };

const routesRecords = [
  { from: 'GRU', to: 'BRC', price: 10 },
  { from: 'GRU', to: 'SCL', price: 18 },
  { from: 'GRU', to: 'ORL', price: 56 },
  { from: 'GRU', to: 'CDG', price: 75 },
  { from: 'SCL', to: 'ORL', price: 20 },
  { from: 'BRC', to: 'SCL', price: 5 },
  { from: 'ORL', to: 'CDG', price: 5 }
];

const getRoutes = () => routesRecords

const createRoute = (route) => {
  routesRecords.push(route);
  return route;
};

module.exports = {
  getRoutes,
  createRoute
};