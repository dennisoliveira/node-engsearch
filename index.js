const express = require('express');
const routesService = require('./service/routes');

const app = express();
const port = 3000;

app.use(express.json());

app.get('/quote/:from/:to', (req, res) => {
  const { from, to } = req.params;
  const lowestRoutePrice = routesService.quoteLowestRoutePrice(from, to);
  if (!lowestRoutePrice) res.status(404).json({ message: 'Route Not Found' });
  return res.json(lowestRoutePrice);
});

app.get('/route', (req, res) => {
  return res.json(routesService.getRoutes());
});

app.post('/route', (req, res) => {

  if (!req.body.from || !req.body.to || !req.body.to) {
    return res.status(400).json({ message: 'Invalid Post Object' });
  }
  
  const body = req.body;
  const createdRoute = routesService.createRoute(body);
  return res.json(createdRoute);
});

if (require.main === module) {
  app.listen(port, () => console.log('Server running localhost:3000...\n'));
}

module.exports = app;