const routesRepository = require('../../repository/routes');

const findDirectRoute = (from, to) => {
  const routes = routesRepository.getRoutes();
  const directRoutes = routes.filter((route) => route.from === from && route.to === to);
  return directRoutes.map((directRoute) => {
    return {
      route: `${directRoute.from},${directRoute.to}`,
      price: directRoute.price
    };
  });
};

const findAllRoutesSameDestination = (to) => {
  const routes = routesRepository.getRoutes();
  return routes.filter((route) => route.to === to);
};

const findAllRoutesSameStart = (from) => {
  const routes = routesRepository.getRoutes();
  return routes.filter((route) => route.from === from);
};

const findAllConnectedRoutes = (from, to) => {
  const routesSameStart = findAllRoutesSameStart(from);
  const routesSameDestination = findAllRoutesSameDestination(to);
  const connectedRoutes = [];

  routesSameStart.forEach((routeStart) => {
    routesSameDestination.forEach((routeDestination) => {
      if (routeStart.to === routeDestination.from) {
        const route = {
          route: `${routeStart.from},${routeDestination.from},${routeDestination.to}`,
          price: routeStart.price + routeDestination.price
        };
        connectedRoutes.push(route);
      }
    });
  });

  return connectedRoutes;
};

const quoteLowestRoutePrice = (from, to) => {
  const allRoutes = [ ...findDirectRoute(from, to), ...findAllConnectedRoutes(from, to)];
  try {
    const lowestRoutePrice = allRoutes.reduce((prevRoute, currentRoute) => 
      prevRoute.price < currentRoute.price
      ? prevRoute
      : currentRoute
    );
    return lowestRoutePrice;
  } catch(error) {
    return null;
  }
};

module.exports = quoteLowestRoutePrice;