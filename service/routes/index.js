const routesRepository = require('../../repository/routes');

const quoteLowestRoutePrice = require('./quoteLowestRoutePrice');

const getRoutes = () => routesRepository.getRoutes();

const createRoute = (route) => routesRepository.createRoute(route);

module.exports = {
  getRoutes,
  createRoute,
  quoteLowestRoutePrice
};